<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class BugManager extends Manager
{

    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["created_at"]);
        $bug->setClosedAt($result["closed"]);

        // Retour
        return $bug;
    }

    public function findAll()
    {
        $dbh = static::connectDb();

        $req_find_all = $dbh->prepare('SELECT * FROM bug');
        $req_find_all->execute();
        
        $bugs = [];

        while($result = $req_find_all->fetch(\PDO::FETCH_ASSOC)) {
            // Instanciation d'un bug FETCH_OBJ
            $bug = new Bug();
            $bug->setId($result["id"]);
            $bug->setTitle($result["title"]);
            $bug->setDescription($result["description"]);
            $bug->setCreatedAt($result["created_at"]);
            $bug->setClosedAt($result["closed"]);
//tableau ASSOC

           /* $bugs[] = [
                "id" => $result["id"],
                "title" => $result["title"],
                "description" => $result["description"],
                "createdAt" => $result["created_at"],
                "closedAt" => $result["closed"],
            ];*/

            array_push($bugs, $bug);
        }

        return $bugs;
    }

    public function add(Bug $bug){

        $dbh = static::connectDb();

        $req_find_all = $dbh->prepare("INSERT INTO bug (`description`, `created_at`, `closed`, `title`) VALUES(:description, :created_at, :closed, :title)"); 
        $req_find_all->bindParam(':description',$bug->getDescription());
        $req_find_all->bindParam(':created_at',$bug->getCreatedAt());
        $req_find_all->bindParam(':title',$bug->getTitle());
        $req_find_all->execute();
        $bugs = [];
    }

    public function update(Bug $bug){

       // Modification d'un incident en BDD

    }







}

<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue

        $content = $this->render('src/Views/Bug/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        $manager = new BugManager();

        $bugs = $manager->findAll();
        // TODO: liste des incidents

        $content = $this->render('src/Views/Bug/list', ['bugs' => $bugs]);

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        // Ajout d'un incident
        if(isset($_POST['action'])){

            $bug = new Bug();
            $bug->setTitle($_POST['first_name']);
            $bug->setCreatedAt($_POST['date']);
            $bug->setDescription($_POST['description']);

            $manager = new BugManager ();
            $manager->add($bug);
        }
        // TODO: ajout d'incident (GET et POST)

        $content = $this->render('src/Views/Bug/add', []);

        return $this->sendHttpResponse($content, 200);
    }


}

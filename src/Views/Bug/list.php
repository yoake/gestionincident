<?php

$bugs = $parameters['bugs'];

?>

<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Starter Template - Materialize</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <link href="../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>

<body>
<?php
  include("menu.php"); ?>
  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <a href="<?= PUBLIC_PATH; ?>bug/add" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>
      <label><a href="<?= PUBLIC_PATH; ?>bug/add">Rapporter un incident</a></label>
      <table>
        <thead>
          <tr>
              <th>id</th>
              <th>Sujet</th>
              <th>Date</th>
              <th>Cloturer</th>
              <th>afficher</th>
          </tr>
        </thead>

        <tbody>
  
        <?php foreach($bugs as $key => $bug) { ?> <pre></pre>
        <tr>
           <td><?= $bug->getId(); ?></td>
            <td><?= $bug->getTitle();?></td>
            <td><?= $bug->getCreatedAt()->format('d/m/Y'); ?></td>
            <td><?php if ($bug->getClosedAt() != null){
              echo $bug ->getClosedAt()->format('d/m/Y');

            }
            else {
              echo "en cours";
            }
            ?></td>
            <td><a href="<?= PUBLIC_PATH; ?>bug/show/<?= $bug->getId(); ?>">Afficher</a></td>
          </tr>
         <?php  } ?>
        </tbody>
      </table>

    </div>
    <br><br>
  </div>

  <?php include("footer.php"); ?>
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/init.js"></script>
</body>
</html>
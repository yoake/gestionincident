<?php

    /** @var $bug \BugApp\Models\Bug */

    $bug = $parameters['bug'];

?>

<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Starter Template - Materialize</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
     <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>

<body>

<?php include("menu.php"); ?>
  <div class="container">
    <div class="section">
      <a href="<?= PUBLIC_PATH; ?>bug" class="waves-effect waves-light btn-flat"><i class="material-icons left">chevron_left</i>Retour</a>
      <div class="row">
        <form class="col s12">
          <div class="row">
            <div class="input-field col s3">
              <p>Nom de l'incident :</p>
            </div>
            <div class="input-field col s3">
              <p><?=$bug->getTitle();?></p>
            </div>
            <div class="input-field col s3">
              <p>Date :</p>
            </div>
            <div class="input-field col s3">
              <p><?php echo $bug->getCreatedAt()->format("d/m/Y");?></p>
            </div>
            
          </div>
          <div class="row">
            <div class="input-field col s6">
              <p>Description :</p>
            </div>
            <div class="input-field col s6">
              <p><?=$bug->getDescription();?></p>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
 
  <?php include("footer.php"); ?>
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/init.js"></script>
</body>

</html>
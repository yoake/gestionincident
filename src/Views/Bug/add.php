<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Starter Template - Materialize</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>

<body>

<?php include("menu.php"); ?>
  <div class="container">
    <h1>Rapport d'incident</h1>
    <div class="section">

      <div class="row">
        <form class="col s12">
          <div class="row">
            <div class="input-field col s6">
              <input id="first_name" name="first_name" type="text" class="validate">
              <label for="first_name">Sujet</label>
            </div>
            <div class="input-field col s6">
              <input id="DAte" name="date" type="date" class="validate">
              <label for="DAte"></label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="description" name="description" type="text" class="validate">
              <label for="first_name">Description</label>
            </div>
          </div>
        
          <button class="btn waves-effect waves-light" type="submit" name="action">Envoyer<i class="material-icons right">send</i>
          </button>
        </form>
      </div>
    </div>
  </div>
  <?php include("footer.php"); ?>
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/init.js"></script>
</body>

</html>